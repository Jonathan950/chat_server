import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.*;

public class ChatServer {
    public static void main(String[] argv) {
        try {
            System.setProperty("java.security.policy", "file:./security.policy");
            Scanner s = new Scanner(System.in);
            System.out.println("Introduzca su nombre y pulse Enter:");
            String name = s.nextLine().trim();

            Chat server = new Chat(name);
            Registry registry = LocateRegistry.createRegistry(8888);
            registry.bind("Chat", server);
            // Naming.rebind("rmi://localhost/ABC", server);

            System.out.println("[Servidor] El chat está listo:");

            while (true) {
                String msg = extracted(s);
                if (server.getClient() != null) {
                    ChatInterface client = server.getClient();
                    msg = "[" + server.getName() + "] " + msg;
                    client.send(msg);
                }
            }

        } catch (Exception e) {
            System.out.println("[System] Server failed: " + e);
        }
    }

    private static String extracted(Scanner s) {
        String msg = s.nextLine().trim();
        return msg;
    }
}